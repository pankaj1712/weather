package com.task.mobiquity.reomote.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.task.mobiquity.weather_details.CityEntity

@Database(entities = [CityEntity::class], version = 1, exportSchema = false)
abstract class MobiquityDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao


    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time. 
        @Volatile
        private var INSTANCE: MobiquityDatabase? = null

        fun getDatabase(context: Context): MobiquityDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MobiquityDatabase::class.java,
                    "Weather_database"
                ).allowMainThreadQueries().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}