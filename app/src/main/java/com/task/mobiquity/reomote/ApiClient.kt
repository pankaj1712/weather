package com.task.mobiquity.reomote


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.facebook.shimmer.BuildConfig
import com.google.gson.GsonBuilder
import com.task.mobiquity.weather_details.weather.CurrentWeatherRes
import com.task.mobiquity.weather_details.forcast.FrocastData
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ApiClient {

    companion object {
        private const val BASE_URL = "https://api.openweathermap.org/"
        private var apiClient: ApiClient? = null
        var apiAuthHelper: ApiAuthHelper? = null
        fun getApiClientIns(): ApiClient {
            if (apiClient == null) {
                getInstance()
                apiClient = ApiClient()
            }
            return apiClient!!
        }

        fun getInstance(): ApiAuthHelper {
            if (apiAuthHelper == null) {
                val httpLoggingInterceptor = HttpLoggingInterceptor()

                if (BuildConfig.DEBUG) {
                    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                } else {
                    // production build
                    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
                }
                val okHttpClient: OkHttpClient = OkHttpClient.Builder()
//                    .callTimeout(30, TimeUnit.SECONDS)
//                    .readTimeout(30, TimeUnit.SECONDS)
//                    .connectTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(httpLoggingInterceptor)
                    .build()
                apiAuthHelper = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(
                        GsonConverterFactory.create(
                            GsonBuilder().serializeNulls().create()
                        )
                    )
                    .build().create(ApiAuthHelper::class.java)
            }
            return apiAuthHelper!!
        }
    }

//    fun checkUser(mobileNumber: String): LiveData<BaseResponse> {
//        val liveData = MutableLiveData<BaseResponse>()
//        if (apiAuthHelper == null) {
//            getInstance()
//        }
//        apiAuthHelper!!.checkUser(mobileNumber).enqueue(object : CallBackManager<BaseResponse>() {
//            override fun onSuccess(response: BaseResponse?) {
//                liveData.value = response!!
//            }
//
//            override fun onFailure(t: Throwable, statusCode: Int) {
//                val response = BaseResponse()
//                response.staus = statusCode
//                response.resMsg = t.localizedMessage!!
//                liveData.value = response
//            }
//
//        })
//        return liveData
//    }

    fun getWatherByCity(lat: Double, lng: Double): LiveData<CurrentWeatherRes> {
        val liveData = MutableLiveData<CurrentWeatherRes>()
        if (apiAuthHelper == null) {
            getInstance()
        }
        apiAuthHelper!!.getWatherByCity("fae7190d7e6433ec3a45285ffcf55c86", lat, lng)
            .enqueue(object : CallBackManager<CurrentWeatherRes>() {
                override fun onSuccess(response: CurrentWeatherRes?) {
                    liveData.value = response!!
                }

                override fun onFailure(t: Throwable, statusCode: Int) {
                    val currentWeatherRes = CurrentWeatherRes()
                    currentWeatherRes.cod = statusCode
                }

            })
        return liveData
    }

    fun getForecastData(lat: Double, lng: Double, unit: String): LiveData<FrocastData> {
        val liveData = MutableLiveData<FrocastData>()
        if (apiAuthHelper == null) {
            getInstance()
        }
        apiAuthHelper!!.getForecastData(lat, lng, "fae7190d7e6433ec3a45285ffcf55c86", unit)
            .enqueue(object : CallBackManager<FrocastData>() {
                override fun onSuccess(response: FrocastData?) {
                    liveData.value = response!!
                }

                override fun onFailure(t: Throwable, statusCode: Int) {
                    val currentWeatherRes = FrocastData()
                    currentWeatherRes.cod = statusCode
                }

            })
        return liveData
    }
}

