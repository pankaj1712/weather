package com.task.mobiquity.reomote

import com.task.mobiquity.weather_details.weather.CurrentWeatherRes
import com.task.mobiquity.weather_details.forcast.FrocastData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiAuthHelper {
    @GET("data/2.5/forecast")
     fun getForecastData(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") string: String,
        @Query("units") unit: String,
    ): Call<FrocastData>


    @GET("data/2.5/weather")
    fun getWatherByCity(
        @Query("appid") string: String,
        @Query("lat") lat: Double,
        @Query("lon") lon: Double): Call<CurrentWeatherRes>
}