package com.task.mobiquity.reomote.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.task.mobiquity.weather_details.CityEntity


@Dao
interface NewsDao {
    @Query("DELETE from cityentity where id=:cityId")
    fun delete(cityId: Int)

    @Query("SELECT * FROM cityentity ORDER BY id DESC")
    fun getAll(): List<CityEntity>

    @Query("SELECT * FROM cityentity where id=:cityId")
    fun getNewsById(cityId: Int): CityEntity?
//this method for pagging lib for jatpack
//    @Query("SELECT * FROM cityentity ORDER BY id DESC")
//    fun getAllNews(): DataSource.Factory<Int, CityEntity>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(cityEntity: CityEntity)

    @Query("DELETE FROM cityEntity")
    fun delete()
}