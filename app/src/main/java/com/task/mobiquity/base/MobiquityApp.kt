package com.task.mobiquity.base

import android.app.Application
import com.task.mobiquity.reomote.local.MobiquityDatabase

class MobiquityApp : Application() {
    override fun onCreate() {
        super.onCreate()
        MobiquityDatabase.getDatabase(this)
    }
}