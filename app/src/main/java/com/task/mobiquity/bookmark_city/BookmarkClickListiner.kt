package com.task.mobiquity.bookmark_city

interface BookmarkClickListiner {
    fun onDeleteClick(cityId: Int)
    fun checkWeatherOfCity(latLngData: LatLngData)
    fun selectLocationFromMap()
}