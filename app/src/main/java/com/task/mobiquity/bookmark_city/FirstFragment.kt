package com.task.mobiquity.bookmark_city

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.task.mobiquity.MapsActivity
import com.task.mobiquity.R
import com.task.mobiquity.databinding.FragmentFirstBinding
import com.task.mobiquity.reomote.local.MobiquityDatabase
import com.task.mobiquity.weather_details.CityEntity


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(R.layout.fragment_first), BookmarkClickListiner {

    lateinit var firstBinding: FragmentFirstBinding
    val list = ArrayList<CityEntity>()
    lateinit var adapter: BookmarkCityAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firstBinding = FragmentFirstBinding.bind(view)
        firstBinding.bookmarkCity.layoutManager = LinearLayoutManager(requireContext())
        adapter = BookmarkCityAdapter(requireContext(), this)
        firstBinding.bookmarkCity.adapter = adapter
        val data = MobiquityDatabase.getDatabase(requireContext()).newsDao().getAll()
        setHasOptionsMenu(true)
        adapter.updateData(data)
    }

    override fun onDeleteClick(cityId: Int) {
        MobiquityDatabase.getDatabase(requireContext()).newsDao().delete(cityId)
    }

    override fun checkWeatherOfCity(latLngData: LatLngData) {
        val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(latLngData)
        findNavController().navigate(action)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.bookmark_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        MobiquityDatabase.getDatabase(requireContext()).newsDao().delete()
        adapter.clearData()
        return super.onOptionsItemSelected(item)
    }

    override fun selectLocationFromMap() {
        val intent = Intent(requireContext(), MapsActivity::class.java)
        startActivityForResult(intent, 555)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 555 && resultCode == Activity.RESULT_OK) {
            val latLngData = data?.getParcelableExtra<LatLngData>("Location")!!
            val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(latLngData)
            findNavController().navigate(action)
        }
    }
}