package com.task.mobiquity.bookmark_city

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LatLngData(val lat:Double, val lng:Double) : Parcelable
