package com.task.mobiquity.bookmark_city

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.task.mobiquity.databinding.BookmarkCityItemBinding
import com.task.mobiquity.databinding.OpenMapItemBinding
import com.task.mobiquity.weather_details.CityEntity

class BookmarkCityAdapter(
    private val context: Context,
    val bookmarkClickListiner: BookmarkClickListiner
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val CITY_ITEM = 999
    val OTHER = 222
    private val list = ArrayList<CityEntity>()
    fun updateData(listData: List<CityEntity>) {
        list.addAll(listData)
        notifyDataSetChanged()
    }

    fun clearData() {
        list.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(
        val bookmarkCityItemBinding: BookmarkCityItemBinding,
        val bookmarkClickListiner: BookmarkClickListiner
    ) :
        RecyclerView.ViewHolder(bookmarkCityItemBinding.root) {

        fun bindData(cityEntity: CityEntity) {
            bookmarkCityItemBinding.cityName.text = cityEntity.name
            bookmarkCityItemBinding.countryNameTv.text = cityEntity.country
            bookmarkCityItemBinding.parentLayoutClick.setOnClickListener {
                val latLngData = LatLngData(cityEntity.lat, cityEntity.lng)
                bookmarkClickListiner.checkWeatherOfCity(latLngData)
            }
            bookmarkCityItemBinding.deleteImg.setOnClickListener {
                list.removeAt(adapterPosition)
                notifyDataSetChanged()
                bookmarkClickListiner.onDeleteClick(cityEntity.id)
            }
        }

    }

    class ViewHolderOpenMap(
        val openMapItemBinding: OpenMapItemBinding,
        val context: Context,
        val bookmarkClickListiner: BookmarkClickListiner
    ) :
        RecyclerView.ViewHolder(openMapItemBinding.root) {

        fun bindData() {
            openMapItemBinding.buttonCity.setOnClickListener {
                bookmarkClickListiner.selectLocationFromMap()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == CITY_ITEM) {
            val binding =
                BookmarkCityItemBinding.inflate(LayoutInflater.from(context), parent, false)
            return ViewHolder(binding, bookmarkClickListiner)
        }
        val binding = OpenMapItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolderOpenMap(binding, context, bookmarkClickListiner)
    }

    override fun getItemViewType(position: Int): Int {
        if (position == list.size) {
            return OTHER
        }
        return CITY_ITEM
    }


    override fun getItemCount(): Int {
        return list.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolderOpenMap) {
            holder.bindData()
        } else if (holder is ViewHolder) {
            holder.bindData(list[position])
        }

    }
}