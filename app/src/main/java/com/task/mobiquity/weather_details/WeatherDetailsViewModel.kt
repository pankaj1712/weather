package com.task.mobiquity.weather_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.task.mobiquity.reomote.ApiClient
import com.task.mobiquity.weather_details.forcast.FrocastData
import com.task.mobiquity.weather_details.weather.CurrentWeatherRes

class WeatherDetailsViewModel : ViewModel() {
    var weatherLiveData: LiveData<CurrentWeatherRes>? = null
    fun getWeatherDetails(lat: Double, lng: Double) {
        weatherLiveData = ApiClient.getApiClientIns().getWatherByCity(lat, lng)
    }

    var forecastLiveData: LiveData<FrocastData>? = null
    fun getForecastDetails(lat: Double, lng: Double) {
        forecastLiveData = ApiClient.getApiClientIns().getForecastData(lat, lng, "metric")
    }

}