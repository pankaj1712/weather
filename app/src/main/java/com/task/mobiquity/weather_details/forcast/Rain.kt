package com.task.mobiquity.weather_details.forcast


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Rain(
    @SerializedName("3h")
    var h: Double = 0.0
)