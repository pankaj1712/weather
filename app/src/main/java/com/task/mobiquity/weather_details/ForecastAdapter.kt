package com.task.mobiquity.weather_details

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.task.mobiquity.databinding.ForecastItemBinding
import com.task.mobiquity.weather_details.forcast.Details
import java.text.DecimalFormat

class ForecastAdapter(var context: Context, val list: ArrayList<Details>) :
    RecyclerView.Adapter<ForecastAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastAdapter.ViewHolder {
        val binding =
            ForecastItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ForecastAdapter.ViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    class ViewHolder(
        val bookmarkCityItemBinding: ForecastItemBinding
    ) :
        RecyclerView.ViewHolder(bookmarkCityItemBinding.root) {
        fun bindData(t: Details) {
            bookmarkCityItemBinding.dateTv.text = t.dtTxt
            bookmarkCityItemBinding.weatherType.text =
                "${t.weather[0].main}, ${t.weather[0].description}"
            bookmarkCityItemBinding.humidityResult.text = "${t.main.humidity}%"
            val temp = t.main.temp
            bookmarkCityItemBinding.tempTv.text =
                "${DecimalFormat("##.##").format(temp)}\u2103"
//            bookmarkCityItemBinding.cityName.text = "${t.name},${t.sys.country}"
            bookmarkCityItemBinding.windSpeedResult.text = "${t.wind.speed} m/s"
            bookmarkCityItemBinding.pressureResult.text = "${t.main.pressure}"
            Glide.with(this.itemView.context)
                .load("http://openweathermap.org/img/wn/${t.weather[0].icon}@2x.png")
                .into(bookmarkCityItemBinding.weatherTypeImg)

        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}


