package com.task.mobiquity.weather_details.forcast


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Wind(
    @SerializedName("deg")
    var deg: Int = 0,
    @SerializedName("gust")
    var gust: Double = 0.0,
    @SerializedName("speed")
    var speed: Double = 0.0
)