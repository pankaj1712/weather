package com.task.mobiquity.weather_details.forcast

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName


@Keep
data class FrocastData(
    @SerializedName("city")
    var city: City = City(),
    @SerializedName("cnt")
    var cnt: Int = 0,
    @SerializedName("cod")
    var cod: Int = 0,
    @SerializedName("list")
    var list: List<Details> = listOf(),
    @SerializedName("message")
    var message: Int = 0
)