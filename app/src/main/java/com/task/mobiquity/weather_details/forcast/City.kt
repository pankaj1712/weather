package com.task.mobiquity.weather_details.forcast


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class City(
    @SerializedName("coord")
    var coord: Coord = Coord(),
    @SerializedName("country")
    var country: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("population")
    var population: Int = 0,
    @SerializedName("sunrise")
    var sunrise: Int = 0,
    @SerializedName("sunset")
    var sunset: Int = 0,
    @SerializedName("timezone")
    var timezone: Int = 0
)