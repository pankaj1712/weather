package com.task.mobiquity.weather_details.forcast

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Details(
    @SerializedName("clouds")
    var clouds: Clouds = Clouds(),
    @SerializedName("dt")
    var dt: Int = 0,
    @SerializedName("dt_txt")
    var dtTxt: String = "",
    @SerializedName("main")
    var main: Main = Main(),
    @SerializedName("pop")
    var pop: Double = 0.0,
    @SerializedName("rain")
    var rain: Rain = Rain(),
    @SerializedName("sys")
    var sys: Sys = Sys(),
    @SerializedName("visibility")
    var visibility: Int = 0,
    @SerializedName("weather")
    var weather: List<Weather> = listOf(),
    @SerializedName("wind")
    var wind: Wind = Wind()
)