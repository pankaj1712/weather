package com.task.mobiquity.weather_details

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.task.mobiquity.R
import com.task.mobiquity.bookmark_city.LatLngData
import com.task.mobiquity.databinding.FragmentWeatherDetailsBinding
import com.task.mobiquity.reomote.local.MobiquityDatabase
import com.task.mobiquity.weather_details.forcast.Details
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.DecimalFormat

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class WeatherDeatilsFragment : Fragment(R.layout.fragment_weather_details) {
    lateinit var fragmentWeatherDetailsBinding: FragmentWeatherDetailsBinding
    lateinit var weatherDetailsViewModel: WeatherDetailsViewModel
    val list = ArrayList<Details>()
    lateinit var forecastAdapter: ForecastAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        fragmentWeatherDetailsBinding = FragmentWeatherDetailsBinding.bind(view)
        weatherDetailsViewModel =
            ViewModelProvider(this).get(WeatherDetailsViewModel::class.java)
        forecastAdapter = ForecastAdapter(requireContext(), list)
        fragmentWeatherDetailsBinding.recyclerViewForecast.layoutManager =
            LinearLayoutManager(requireContext())
        fragmentWeatherDetailsBinding.recyclerViewForecast.adapter = forecastAdapter
        getWeatherData()
//        view.findViewById<Button>(R.id.button_second).setOnClickListener {
//            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
//        }
    }

    private fun getWeatherData() {
        val latLng = requireArguments().getParcelable<LatLngData>("data")!!

        weatherDetailsViewModel.getWeatherDetails(latLng.lat, latLng.lng)
        weatherDetailsViewModel.weatherLiveData?.observe(viewLifecycleOwner,
            { t ->
                if (t?.cod == 200) {

                    fragmentWeatherDetailsBinding.weatherType.text =
                        "${t.weather[0].main}, ${t.weather[0].description}"
                    fragmentWeatherDetailsBinding.humidityResult.text = "${t.main.humidity}%"
                    val temp = t.main.temp - 273.15
                    fragmentWeatherDetailsBinding.tempTv.text =
                        "${DecimalFormat("##.##").format(temp)}\u2103"
                    fragmentWeatherDetailsBinding.cityName.text = "${t.name},${t.sys.country}"
                    fragmentWeatherDetailsBinding.windSpeedResult.text = "${t.wind.speed} m/s"
                    fragmentWeatherDetailsBinding.pressureResult.text = "${t.main.pressure}"
                    Glide.with(this)
                        .load("http://openweathermap.org/img/wn/${t.weather[0].icon}@2x.png")
                        .into(fragmentWeatherDetailsBinding.weatherTypeImg)
                    if (!t.name.isNullOrEmpty() && t.id > 0) {
                        val cityEntity = CityEntity()
                        cityEntity.country = t.sys.country
                        cityEntity.id = t.id
                        cityEntity.name = t.name
                        cityEntity.lat = t.coord.lat
                        cityEntity.lng = t.coord.lon
                        bookMarkCity(cityEntity)
                    }

                }
            })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.weather_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_forecast -> {

                fragmentWeatherDetailsBinding.cardView.visibility = View.GONE
                fragmentWeatherDetailsBinding.recyclerViewForecast.visibility = View.VISIBLE
                if (list.isEmpty()) {
                    getForecast()
                    return true
                }
            }
            R.id.action_weather -> {
                fragmentWeatherDetailsBinding.cardView.visibility = View.VISIBLE
                fragmentWeatherDetailsBinding.recyclerViewForecast.visibility = View.GONE
                getWeatherData()

            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getForecast() {
        val latLng = requireArguments().getParcelable<LatLngData>("data")!!
        weatherDetailsViewModel.getForecastDetails(latLng.lat, latLng.lng)
        weatherDetailsViewModel.forecastLiveData?.observe(viewLifecycleOwner,
            { t ->
                if (t?.cod == 200) {
                    list.clear()
                    list.addAll(t.list)
                    forecastAdapter.notifyDataSetChanged()

                }
            })
    }

    private fun bookMarkCity(cityEntity: CityEntity) {
        GlobalScope.launch {
            MobiquityDatabase.getDatabase(requireContext()).newsDao().insertAll(cityEntity)

            Log.d(
                "DAtaSize",
                "${MobiquityDatabase.getDatabase(requireContext()).newsDao().getAll().size}"
            )
        }
    }


}