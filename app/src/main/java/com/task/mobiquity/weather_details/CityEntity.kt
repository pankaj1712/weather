package com.task.mobiquity.weather_details

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(indices = [Index(value = ["id"], unique = true)])
data class CityEntity(

    @SerializedName("country")
    var country: String = "",
    //city ID
    @PrimaryKey
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    var lat: Double = 0.0,
    var lng: Double = 0.0
)