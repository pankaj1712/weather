package com.task.mobiquity.weather_details.forcast


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Clouds(
    @SerializedName("all")
    var all: Int = 0
)